<?php
/**
 * 
 * The template used for displaying page content in template-ejercicio.php
 *
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="post-content">
	<?php
		$hide_title = get_post_meta( get_the_ID(), '_genesis_block_theme_hide_title', true );
	if ( ! $hide_title ) {
		?>
		<header class="entry-header">
			<h1 class="entry-title text-center">
				<?php the_title(); ?>
			</h1>
		</header>
		<?php
	} // End if hide title.
?>
		<div id="app">
			<div class="mx-auto mb-4">
				<div class="grid grid-cols-1 md:grid-cols-3 gap-4">
					<div>
						<label for="grupo-familia">Grupo de familia</label>
						<select class="p-2 h-20" id="grupo-familia" name="grupo-familia" v-model="groupFamilySelected" @change="onChangeFamilyGroup($event)" >
							<option value="" disabled selected>Selecciona</option>
							<option v-for="groupFamily in groupFamilies" :key="groupFamily.id" :value="groupFamily.id">{{ groupFamily.name }}</option>
						</select>
					</div>
					<div>
						<label for="familia">Familia</label>
						<select class="p-2 h-20" id="familia" name="familia" v-model="familySelected" @change="onChangeFamily($event)" id="familia">
							<option value="" disabled selected>Selecciona</option>
							<option v-for="family in families" key="family.id" :value="family.id">{{family.name }}</option>
						</select>
					</div>
					<div>
						<label for="subfamilia">Subfamilia</label>
						<select class="p-2 h-20" id="subfamilia "name="subfamilia" v-model="subFamilySelected" @change="onChangeFilter($event)" id="subfamilia">
							<option value="" disabled selected>Selecciona</option>
							<option v-for="subFamily in subFamilies" :key="subFamily.id" :value="subFamily.id">{{ subFamily.name }}</option>
						</select>
					</div>
				</div>
			</div>	
			<table class="mt-8 table-auto" v-if="providers !== null && providers !== ''  && providers != 'No providers'">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Logo</th>
					</tr>
				</thead>
				<tbody>
					<tr v-for="provider in providers" :key="provider.id" :value="provider.id">
						<td>{{ provider.commercial_name }}</td>
						<td><img width="100" :src="provider.logo" :alt="provider.commercial_name"></td>
					</tr>
				</tbody>
			</table>
			<div v-if="providers === 'No providers'">
				<p>No hay proveedores</p>
			</div>
		</div>
	</div><!-- .post-content-->
</article><!-- #post-## -->

<script>
	/* VUE app */
	let app = {
		data() {
			return {
				groupFamilySelected: '',
				familySelected: '',
				subFamilySelected: '',
				token: '',
				groupFamilies: '',
				families:'',
				subFamilies: '',
				providers:''
			}
		},

		created() {
			/* Get token and Family Groups */
			getToken('soporte@grupoibricks.com', 'Sg2023$$')
				.then(data => {
					console.log('Token:', data.token);
					this.token = data.token;
					connectAPI(data.token, 'https://api-brick.kkkk1234.com/api/v1/group-families')
						.then(data => {
							console.log('Group Families:', data);
							this.groupFamilies = data.groupFamilies
						})
						.catch(error => {
							console.error('Error:', error);
						});						
				})
		},
		methods: {
			/* Get families */
			onChangeFamilyGroup(event) {
				let id = event.target.value;		
				let url = `https://api-brick.kkkk1234.com/api/v1/group-families/${id}/families`

				connectAPI(this.token, url)
					.then(data => {
						console.log('Families:', data);
						this.families = data.families
						this.subFamilies = ''
						this.providers = ''
					})
					.catch(error => {
						console.error('Error:', error);
					});
			},

			/* Get Subfamilies*/
			onChangeFamily(event) {
				let id = event.target.value;
				let url = `https://api-brick.kkkk1234.com/api/v1/families/${id}/sub-families`

				connectAPI(this.token, url)
					.then(data => {
						console.log('SubFamilies:', data);
						this.subFamilies = data.subFamilies;
						this.providers = '';
					})
					.catch(error => {
						console.error('Error:', error);
					});
			},

			/* Filter and get providers */
			onChangeFilter(event) {
				let subFamily = event.target.value;
				let url = `https://api-brick.kkkk1234.com/api/v1/providers?family_id=${this.familySelected}&group_family_id=${this.groupFamilySelected}&sub_family_id=${subFamily}&commercial_name=er&page=1&items_per_page=20`;
				
				connectAPI(this.token, url)
					.then(data => {
						console.log('Filter:', data.providers);
						if (data.providers.length > 0){
							this.providers = data.providers;
						}
						else {
							this.providers = 'No providers';
						}
					})
					.catch(error => {
						console.error('Error:', error);
					});
			}
			
		},
	};

	Vue.createApp(app).mount('#app');

</script>

