/*
* Function to get token from API
*
*/

async function getToken(email, password) {
    const url = 'https://api-brick.kkkk1234.com/api/v1/login';
    const data = {
        email: email,
        password: password
    };
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    if (!response.ok) {
        throw new Error('Response failed');
    }
    const jsonResponse = await response.json();
    return jsonResponse;
}


/*
* Function to connect to API
*
*/
  
async function connectAPI(token, url) {
  
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        'Authorization': 'Bearer ' + token,
        'Content-Type': 'application/json'
      }
    });
  
    if (!response.ok) {
      throw new Error('Resonse failed');
    }
  
    const jsonResponse = await response.json();
    return jsonResponse;
  }
 

