<?php

/**
 * Enqueue styles and script for theme
 * 
 * In Page Ejercicio enqueue: VUE, Tailwind, and API connection js
 * 
 */

function ibricks_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . "/style.css");
    wp_enqueue_style( 'ibrick', get_stylesheet_uri());

    if (is_page('ejercicio')) {
        wp_enqueue_script( 'vue', 'https://unpkg.com/vue@3/dist/vue.global.js');
        wp_enqueue_script( 'tailwind', 'https://cdn.tailwindcss.com');
        wp_enqueue_script( 'api', get_stylesheet_directory_uri() . "/js/api.js");
    }
}

add_action( 'wp_enqueue_scripts', 'ibricks_enqueue_styles' );
