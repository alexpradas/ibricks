# Ejercicio Ibricks

Tema hijo de Wordpress creado para ejercicio Ibricks. 

[Link Ejercicio ](https://pruebaalejadev.wpenginepowered.com/)


Tecnologías utilizadas:

- VUE
- Tailwind

Seleccionar: Grupo de Familia, Familia, y Subfamilia

Una vez seleccionada la Subfamilia se genera la tabla de proveedores

Ejemplo:

- Grupo de Familia: Pavimento, revestimiento, baño y cocina
- Familia: Mosaicos y similares
- Subfamilia: Mosaicos y similares


Tabla Proveedores

- Nombre
- Logo

<img src="screenshot.png" style="zoom:80%;" />
