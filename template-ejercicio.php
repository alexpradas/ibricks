<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * Template Name: Ejercicio
 */

get_header(); ?>

	<div id="" class="content-area">
		<main id="main" class="site-main">

			<?php
			while ( have_posts() ) :
				the_post();

				// Page content template.
				get_template_part( 'template-parts/content-page-ejercicio' );

			endwhile;
			?>

		</main><!-- #main -->
	</div><!-- #primary -->

	<?php get_footer(); ?>
